#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import contextlib
from io import StringIO
import os
import sys
import unittest

from unittest.mock import patch

import sortformats

this_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.join(this_dir, '..')


class TestLowerThan(unittest.TestCase):

    def test_1(self):
        islower = sortformats.lower_than("raw", "bmp")
        self.assertEqual(True, islower)

    def test_2(self):
        islower = sortformats.lower_than("eps", "bmp")
        self.assertEqual(False, islower)

    def test_3(self):
        islower = sortformats.lower_than("eps", "eps")
        self.assertEqual(False, islower)


class TestSortPivot(unittest.TestCase):

    def test_1(self):
        formats = ["raw", "bmp", "tiff"]
        sortformats.sort_pivot(formats, 1)
        self.assertEqual(["raw", "bmp", "tiff"], formats)

    def test_2(self):
        formats = ["raw", "bmp", "tiff", "jpeg", "gif"]
        sortformats.sort_pivot(formats, 1)
        self.assertEqual(["raw", "bmp", "tiff", "jpeg", "gif"], formats)

    def test_3(self):
        formats = ["raw", "tiff", "jpeg", "bmp", "gif"]
        sortformats.sort_pivot(formats, 3)
        self.assertEqual(["raw", "bmp", "tiff","jpeg", "gif"], formats)

    def test_4(self):
        formats = ["raw", "tiff", "gif", "jpeg", "bmp"]
        sortformats.sort_pivot(formats, 4)
        self.assertEqual(["raw", "bmp", "tiff", "gif", "jpeg"], formats)


class TestSortFormats(unittest.TestCase):

    def test_1(self):
        formats = ["bmp", "tiff", "jpeg", "gif", "raw"]
        expected = ["raw", "bmp", "tiff", "gif", "jpeg"]
        ordered = sortformats.sort_formats(formats)
        self.assertEqual(expected, ordered)

    def test_2(self):
        formats = ["raw", "bmp", "tiff", "tiff", "jpeg", "gif", "raw"]
        expected = ["raw", "raw", "bmp", "tiff", "tiff", "gif", "jpeg"]
        ordered = sortformats.sort_formats(formats)
        self.assertEqual(expected, ordered)


class TestMain(unittest.TestCase):

    def test_simple(self):
        expected = "raw bmp tiff gif jpeg\n"
        stdout = StringIO()
        with patch.object(sys, 'argv',
                          ["sortformats.py", "raw", "bmp", "tiff",
                           "jpeg", "gif"]):
            with contextlib.redirect_stdout(stdout):
                sortformats.main()
            output = stdout.getvalue()
            self.assertEqual(expected, output)

    def test_repeated(self):
        expected = "raw raw bmp tiff tiff gif jpeg\n"
        stdout = StringIO()
        with patch.object(sys, 'argv',
                          ["sortformats.py", "raw", "bmp", "tiff",
                           "tiff", "jpeg", "gif", "raw"]):
            with contextlib.redirect_stdout(stdout):
                sortformats.main()
            output = stdout.getvalue()
            self.assertEqual(expected, output)

    def test_badformat(self):
        expected = "raw raw tiff tiff gif jpeg\n"
        stdout = StringIO()
        with patch.object(sys, 'argv',
                          ["sortformats.py", "raw", "bmp", "tiff",
                           "badformat", "tiff", "jpeg", "gif", "raw"]):
            with self.assertRaises(SystemExit):
                sortformats.main()


if __name__ == '__main__':
    unittest.main(module=__name__, buffer=True, exit=False)
